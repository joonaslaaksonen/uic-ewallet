import React, { Component } from 'react';
import {Modal, Grid, Row, Col, Button} from 'react-bootstrap';

import '../css/ModalContainer.css';

class ModalContainer extends Component {
  render() {
    return (
      <Modal show={this.props.show} onHide={this.props.close}>
          <Modal.Header closeButton>
            <Modal.Title>{this.props.modalHeaderText}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Grid>
              <Row>
                <Col xs={10}>
                  <span>{this.props.modalBodyText}</span>
                </Col>
                <Col xs={2}>
                  <span>{this.props.modalNumberValue}</span>
                </Col>
              </Row>
            </Grid>
          </Modal.Body>
          <Modal.Footer>
            <Grid>
              {this.props.confirmCallback ?
              <Row>
                <Col xs={8}>
                  <Button onClick={this.props.confirmCallback} className="confirm">CONFIRM</Button>
                </Col>
                <Col xs={4}>
                  <Button onClick={this.props.close} className="cancel button-decline">CANCEL</Button>
                </Col>
              </Row>
              :
              <Row>
                <Col xs={12}>
                  <Button onClick={this.props.close} className="cancel button-decline">CLOSE</Button>
                </Col>
              </Row>
              }
            </Grid>
          </Modal.Footer>
      </Modal>
    );
  }
}

export default ModalContainer;