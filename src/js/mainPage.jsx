import React, { Component } from 'react';
import {Grid, Row, Col} from 'react-bootstrap';
import CommunityBar from './CommunityBar';
import RecentTransactions from './RecentTransactions';
import PendingRequests from './PendingRequests';
import SendRequestButton from './SendRequestButton';
import '../css/MainPage.css';

class MainPage extends Component {
  render() {
    const {community, currentBalance, transactions} = this.props.data;
    const AccBalance = () => (
      <Row className="show-grid">
        <Col xs={10} sm={10} className="text-left">
          Account balance in {community}
        </Col>
        <Col cs={2} sm={2} className="text-right">
          {currentBalance}
        </Col>
      </Row>
    );

    // If just logged in, select community
    if(community === null){
      this.props.history.push('/communities');
    }

    return (
      <div className="main-page">
        <Grid fluid={true}>
          <CommunityBar community={community} />
            <AccBalance />
            <RecentTransactions transactions={transactions}/>
            <PendingRequests
              transactions={transactions}
              updateBalance={this.props.updateBalance}

              showModal={this.props.showModal}
              acceptRequest={this.props.acceptRequest}
              declineRequest={this.props.declineRequest}
              />
        </Grid>
        <SendRequestButton />
      </div>
    );
  }
}


export default MainPage;
