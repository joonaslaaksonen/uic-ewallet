import React, { Component } from 'react';

import '../css/Footer.css';

class Footer extends Component {
  render() {
    return (
      <div className="footer">
        <p>Copyright Timewallet Corporation 2017</p>
      </div>
    );
  }
}

export default Footer;