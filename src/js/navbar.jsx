import React, { Component } from 'react';
import { Link} from 'react-router-dom'
import {Nav, Navbar as NavbarBootstrap, NavItem} from 'react-bootstrap';
import {LinkContainer} from 'react-router-bootstrap';
import '../css/NavBar.css';

class Navbar extends Component {
  render() {
    let showMenu = window.location.pathname !== "/login";
    return(
      <NavbarBootstrap collapseOnSelect>
        <NavbarBootstrap.Header>
          <NavbarBootstrap.Brand>
            <Link to={"/"}>Timewallet</Link>
          </NavbarBootstrap.Brand>
          {showMenu &&
          <NavbarBootstrap.Toggle />
          }
        </NavbarBootstrap.Header>

        <NavbarBootstrap.Collapse>
          <Nav>
          <LinkContainer to="/transactions">
              <NavItem eventKey={1}>Resent transactions</NavItem>
            </LinkContainer>
            <LinkContainer to="/transfer">
              <NavItem eventKey={2}>Send or Request Time</NavItem>
            </LinkContainer>
            <LinkContainer to="/profile">
              <NavItem eventKey={3}>My profile</NavItem>
            </LinkContainer>
            <LinkContainer to="/communities">
              <NavItem eventKey={4}>Change community</NavItem>
            </LinkContainer>
            <LinkContainer to="/login">
              <NavItem eventKey={5}>Log out</NavItem>
            </LinkContainer>
          </Nav>
        </NavbarBootstrap.Collapse>

      </NavbarBootstrap>
    );
  }
}

export default Navbar;
