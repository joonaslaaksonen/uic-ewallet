import React, {Component} from 'react';
import {Grid, Row, Col} from 'react-bootstrap';
import Request from './Request';
import NavBackBar from './NavBackBar.jsx';

class AllRequests extends Component {
  createElements() {
    const {transactions} = this.props.data;
    let pending = [];
    transactions.forEach((t) => {
      if (t.type === 'pending') {
        pending.push(t);
      }
    });
    const pendingElems = pending.map((data, index) => (
      <Request
        data={data}
        key={index}
        showModal={this.props.showModal}
        declineRequest={this.props.declineRequest}
        acceptRequest={this.props.acceptRequest}
        />
    ));
    return pendingElems;
  }

  render() {
    const transactionElems = this.createElements();

    return(
      <Grid>
        <NavBackBar currentPage={"Requests"}/>
        {transactionElems}
      </Grid>
    );
  }
}

export default AllRequests;
