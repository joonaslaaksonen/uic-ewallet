import React, {Component} from 'react';
import {Grid, Row, Col} from 'react-bootstrap';
import {Button} from 'react-bootstrap';
import {LinkContainer} from 'react-router-bootstrap';

class SendRequestButton extends Component {
  render() {
    return(
      <Grid>
        <Row className="send-request-button">
          <Col xs={10} xsOffset={1} sm={12}>
            <LinkContainer to='/transfer'>
              <Button bsSize="large" block={true}
                className="button-accept">Send/Request</Button>
            </LinkContainer>
          </Col>
        </Row>
      </Grid>
    );
  }
}

export default SendRequestButton;
