import React, {Component} from 'react';
import {Row, Col} from 'react-bootstrap';
import {LinkContainer} from 'react-router-bootstrap';
import AngleLeft from 'react-icons/lib/fa/angle-left';
import '../css/NavBackBar.css';

class NavBackBar extends Component {
  render() {
    return(
        <Row className="show-grid nav-back-bar">
          <Col xs={2} sm={2} className="angle">
            <LinkContainer to="/">
              <AngleLeft size={28}/>
            </LinkContainer>
          </Col>
          <Col xs={10} sm={10} className="text">
            <span>{this.props.currentPage ? this.props.currentPage : "test"}</span>
          </Col>
        </Row>
    );
  }
}

export default NavBackBar;
