import React, { Component } from 'react';
import {Redirect} from 'react-router-dom'
import {Row, Col, FormGroup, FormControl, Button, Form} from 'react-bootstrap';

import '../css/LoginPage.css';

class LoginPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      redirectToReferrer: false,
      username: "",
      password: "",
    }
    this.login = this.login.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleChange(e) {
    const name = e.target.name;
    const value = e.target.value;
    this.setState({
      [name]: value,
    });
  }
  handleSubmit(e) {
    e.preventDefault();
    this.login()
  }
  login = () => {
    this.props.fakeAuth.authenticate(() => {
      this.setState({ redirectToReferrer: true })
    })
  }
  render() {
    if (this.state.redirectToReferrer) {
      return (
        <Redirect to={this.props.location}/>
      )
    }
    return (
      <Row className="login-page">
        <Col xs={10} xsOffset={1}>
          <Form onSubmit={this.handleSubmit}>
            <FormGroup>
              <FormControl
                type="text"
                name="username"
                value={this.state.username}
                placeholder="Email address"
                onChange={this.handleChange}
              />
            </FormGroup>
            <FormGroup>
              <FormControl
                type="password"
                name="password"
                value={this.state.password}
                placeholder="Password"
                onChange={this.handleChange}
              />
            </FormGroup>
              <Button type="submit">
                LOGIN
              </Button>
          </Form>
        </Col>
      </Row>
    );
  }
}

export default LoginPage;