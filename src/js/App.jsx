import React, { Component } from 'react';
import Navbar from './navbar.jsx';
import Footer from './Footer.jsx';
import FeedbackContainer from './FeedbackContainer.jsx';
import ModalContainer from './ModalContainer.jsx';

import jsonData from '../data/data.json';

class App extends Component {
  constructor(props) {
      super(props);
      this.state = {
          community: null,
          communities: null,
          account: this.props.account,
          balance: null,
          currentBalance: null,
          transactions: null,
          name: null,
          email: null,
          feedbacks: [],  //example syntax [{message:"test", type:"info", id:"1234"}]

          showModal: false,
          modalHeaderText: "",
          modalBodyText: "",
          modalNumberValue: "",
          confirmCallback: null,
      }
      this.sendTime = this.sendTime.bind(this);
      this.createRequest = this.createRequest.bind(this);
      this.updateCommunity = this.updateCommunity.bind(this);
      this.createFeedback = this.createFeedback.bind(this);
      this.removeFeedback = this.removeFeedback.bind(this);
      this.closeModal = this.closeModal.bind(this);
      this.showModal = this.showModal.bind(this);
      this.updateBalance = this.updateBalance.bind(this);
      this.acceptRequest = this.acceptRequest.bind(this);
      this.declineRequest = this.declineRequest.bind(this);
      this.updateName = this.updateName.bind(this);
      this.updateEmail = this.updateEmail.bind(this);
  }

  componentDidMount() {
    let acc;
    jsonData.accounts.forEach((a) => {
      if (a.id === this.props.account) {
        acc = a;
      }
    });
    this.setState({
      communities: jsonData.communities,
      balance: acc.balance,
      currentBalance: acc.balance[this.state.community],
      transactions: acc.transactions,
      name: acc.name,
      email: acc.email
    });
  }

  updateName(value) {
    this.setState({
      name: value
    });
  }

  updateEmail(value) {
    this.setState({
      email: value
    });
  }

  updateCommunity(value) {
    this.setState({
      community: value,
      currentBalance: this.state.balance[value]
    });
  }

  updateBalance(value) {
    const newValue = parseFloat(this.state.currentBalance) + value;
    let balance = Object.assign({}, this.state.balance);
    balance[this.state.community] = newValue
    return this.setState({
      balance,
      currentBalance: newValue
    });
  }

  createTransaction(title, amount, type, user) {
    let localTransactions = this.state.transactions.slice();
    let today = new Date();
    let tempDate = [today.getDate(), today.getMonth()+1, today.getFullYear()]
    localTransactions.push({
      title:title,
      type: type,
      amount: amount,
      with: user,
      date: tempDate.join("."),
      id: localTransactions.length + 1,
    })
    this.setState({
      transactions: localTransactions,
    })
  }

  sendTime(hours, title, user) {
    this.updateBalance(-hours);
    this.createTransaction(title, hours, "sent", user);

    // Show feedback modal with correct info
    this.showModal(
      null,
      "Payment processed",
      hours + " hours sent to " + user+ " for completing " + title + "!",
      hours,
    )
  }

  createRequest(hours, title, user) {
    // This will only show feedback.
    this.showModal(
      null,
      "Request sent",
      hours + " hours requested from " + user+ " for completing " + title + "!",
      hours,
    )
  }

  acceptRequest(id) {
    // Find request that was accepted and change status
    let transactions = this.state.transactions.slice();
    let today = new Date();
    let todayAsString = [today.getDate(), today.getMonth()+1, today.getFullYear()].join(".");
    let indexOfTransaction = 0
    transactions.forEach((t,idx) => {
      if (t.id === id) {
        t.type = "sent";
        t.date = todayAsString;
        indexOfTransaction = idx;
      }
    });
    this.setState({
      transactions: transactions
    });
    this.updateBalance(-transactions[indexOfTransaction].amount);

    // Show feedback modal with correct info
    this.showModal(
      null,
      "Payment processed",
      transactions[indexOfTransaction].amount + " hours sen to " + transactions[indexOfTransaction].with + " for completing " + transactions[indexOfTransaction].title + "!",
      transactions[indexOfTransaction].amount,
    )
  }

  declineRequest(id) {
    // Find request that was declined and change status
    let transactions = this.state.transactions.slice();
    let today = new Date();
    let todayAsString = [today.getDate(), today.getMonth()+1, today.getFullYear()].join(".");
    let indexOfTransaction = 0
    transactions.forEach((t,idx) => {
      if (t.id === id) {
        t.type = "declined";
        t.date = todayAsString;
        indexOfTransaction = idx;
      }
    });
    this.setState({
      transactions: transactions
    });
    // Show feedback modal with correct info
    this.showModal(
      null,
      "Request declined",
      "Request from " + transactions[indexOfTransaction].with + " for completing " + transactions[indexOfTransaction].title + " declined.",
      "",
    )
  }

  createFeedback(message, type) {
    // unix timestamp used as ID which is needed when removing feedback
    let localFeedback = this.state.feedbacks.slice();
    localFeedback.push({
      message: message,
      type: type,
      id:  Math.round((new Date()).getTime() / 1000),
    });
    this.setState({
      feedbacks: localFeedback,
    })
  }

  removeFeedback(id) {
    let idx = -1;
    this.state.feedbacks.find((element, index) => {
      if(element.id === id) {
        idx = index;
        return true;
      }
    });
    if (idx < 0 ) {return;}
    let localFeedback = this.state.feedbacks.slice();
    localFeedback.splice(idx, 1);
    this.setState({
      feedbacks: localFeedback,
    })
  }
  showModal(confirmCallback, header="", body="", value="") {
    this.setState({
      showModal: true,
      confirmCallback: confirmCallback,
      modalHeaderText: header,
      modalBodyText: body,
      modalNumberValue: value,
    })
  }

  closeModal() {
    this.setState({
      showModal: false,
    })
  }

  render() {
    const childrenWithProps = React.Children.map(this.props.children, child =>
      React.cloneElement(child, {
        data:this.state,
        updateCommunity: this.updateCommunity,
        sendTime: this.sendTime,
        createRequest: this.createRequest,
        createFeedback: this.createFeedback,
        showModal: this.showModal,
        updateBalance: this.updateBalance,
        acceptRequest: this.acceptRequest,
        declineRequest: this.declineRequest,
        updateName: this.updateName,
        updateEmail: this.updateEmail
      }));

    return (
      <div className="app">
        <Navbar />
        {childrenWithProps}
        <Footer />
        {this.state.feedbacks.length > 0 ?
          <FeedbackContainer
            feedbacks={this.state.feedbacks}
            removeFeedback={this.removeFeedback}
          />
        :
        false
        }
        <ModalContainer
          show = {this.state.showModal}
          close = {this.closeModal}
          confirmCallback = {this.state.confirmCallback}
          modalHeaderText = {this.state.modalHeaderText}
          modalBodyText = {this.state.modalBodyText}
          modalNumberValue = {this.state.modalNumberValue}
        />

      </div>
    );
  }
}

export default App;
