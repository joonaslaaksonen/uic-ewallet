import React, {Component} from 'react';
import {Row, Col} from 'react-bootstrap';
import Request from './Request';
import AngleRight from 'react-icons/lib/fa/angle-right';
import {LinkContainer} from 'react-router-bootstrap';

class PendingRequests extends Component {
  createElements() {
    const {transactions} = this.props;
    let pending = [];
    transactions.forEach((t) => {
      if (t.type === 'pending') {
        pending.push(t);
      }
    });
    // pending.reverse();
    const n = pending.length < 3 ? pending.length : 3;
    pending = pending.slice(0, n);
    const pendingElems = pending.map((data, index) => (
      <Request
        data={data}
        key={index}
        showModal={this.props.showModal}
        declineRequest={this.props.declineRequest}
        acceptRequest={this.props.acceptRequest}
        />
    ));
    return pendingElems;
  }

  render() {
    const pendingElems=this.createElements();

    return(
      <div>
        <Row>
          <Col xs={12} sm={12} className="text-left">
            <strong>Pending requests</strong>
          </Col>
        </Row>
        {pendingElems}
        <LinkContainer to="/requests">
          <Row>
            <Col xs={6} sm={6} className="text-left">
              More requests
            </Col>
            <Col xs={6} sm={6} className="text-right">
              <AngleRight size={28}/>
            </Col>
          </Row>
        </LinkContainer>
      </div>
    );
  }
}

export default PendingRequests;
