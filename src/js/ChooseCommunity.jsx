import React, {Component} from 'react';
import {Grid, Row, Col} from 'react-bootstrap';
import {LinkContainer} from 'react-router-bootstrap';
import AngleRight from 'react-icons/lib/fa/angle-right';
import NavBackBar from './NavBackBar.jsx';

class ChooseCommunity extends Component {
  render() {
    const communities = this.props.data.communities;
    const communityElems = communities.map((name, index) => (
      <LinkContainer to="/" key={index}>
        <Row onClick={() => this.props.updateCommunity(name)}>
          <Col xs={10} sm={10} className="text-left">
            {name}
          </Col>
          <Col xs={2} sm={2} className="text-right">
            <AngleRight size={28}/>
          </Col>
        </Row>
      </LinkContainer>
    ));
    return(
      <Grid fluid={true}>
        {this.props.data.community ?
        <NavBackBar currentPage={"Choose community"}/>
        :
        <Row className="show-grid community-bar">
          <Col xs={12} sm={12} className="text-left">
            <strong>Welcome! Choose a community!</strong>
          </Col>
        </Row>
        }
        {communityElems}
      </Grid>
    )
  }
}

export default ChooseCommunity;
