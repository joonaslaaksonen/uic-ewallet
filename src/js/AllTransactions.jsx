import React, {Component} from 'react';
import {Grid, Row, Col} from 'react-bootstrap';
import Transaction from './Transaction';
import NavBackBar from './NavBackBar.jsx';

class AllTransactions extends Component {
  createElements() {
    const {transactions} = this.props.data;
    let recents = []
    // transactions.reverse();
    transactions.forEach((t) => {
      if (t.type !== 'pending') {
        recents.push(t);
      }
    });

    recents.sort(this.compareTransactions);
    const transactionElems = recents.map((data, index) => {
      if (data.type !== 'pending') {
        return <Transaction data={data} key={index} />;
      }
    });
    return transactionElems;
  }

  compareTransactions(a,b) {
    // Compares date properties of parameters. Date properties must be in form "dd.mm.yyyy"
    let dateA = new Date(a.date.split('.')[2], a.date.split('.')[1],a.date.split('.')[0]);
    let dateB = new Date(b.date.split('.')[2], b.date.split('.')[1],b.date.split('.')[0]);
    if(dateA > dateB) {
      return -1;
    }
    if(dateA < dateB){
      return 1;
    }
    return 0;
  }

  render() {
    const transactionElems = this.createElements();
    return(
      <Grid>
        <NavBackBar currentPage={"Transactions"}/>        
        {transactionElems}
      </Grid>
    );
  }
}

export default AllTransactions;
