import React, { Component } from 'react';
import {Row, Col} from 'react-bootstrap';
import Feedback from './Feedback.jsx';


import '../css/FeedbackContainer.css';

class FeedbackContainer extends Component {
  render() {
    let feedbacks = [];
    this.props.feedbacks.forEach((elem, idx)=> {
      feedbacks.push(
        <Feedback
          key ={elem.id}
          message = {elem.message}
          type= {elem.type}
          id = {elem.id}
          removeFeedback = {this.props.removeFeedback}
        />)
    })

    return (
      <Row className="feedback-container">
        <Col xs={11} xsOffset={1}>
          {feedbacks}
        </Col>
      </Row>
    );
  }
}

export default FeedbackContainer;