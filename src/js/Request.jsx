import React, {Component} from 'react';
import {Row, Col, Button, Label} from 'react-bootstrap';
import ModalContainer from  './ModalContainer';

class Request extends Component {
  constructor(props) {
    super(props)
    this.state = {
      showModal: false
    };

    this.openAcceptRequestModal = this.openAcceptRequestModal.bind(this);
    this.openDeclineRequestModal = this.openDeclineRequestModal.bind(this);
    this.localAcceptRequest = this.localAcceptRequest.bind(this);
    this.localDeclineRequest = this.localDeclineRequest.bind(this);
  }

  openAcceptRequestModal() {
    this.props.showModal(
      this.localAcceptRequest,
      "Confirm your payment",
      "Sending " + this.props.data.amount + " hours to " + this.props.data.with + " for completing " + this.props.data.title,
      this.props.data.amount
    );
  }
  openDeclineRequestModal() {
    this.props.showModal(
      this.localDeclineRequest,
      "Confirm your decline",
      "Declining " + this.props.data.with + "'s request for completing " + this.props.data.title
    );
  }
  localAcceptRequest() {
    // Helper function to allow passing parameter to modals callback
    this.props.acceptRequest(this.props.data.id);
  }
  localDeclineRequest() {
    // Helper function to allow passing parameter to modals callback
    this.props.declineRequest(this.props.data.id);
  }

  render() {
    return(
      <div>
        <Row className="show-grid no-border">
          <Col sm={10} xs={10} className="text-left">
            {this.props.data.with} has requested {this.props.data.amount} hours
            from you for {this.props.data.title}
          </Col>
          <Col sm={2} xs={2} className="text-right">
            {<h3><Label bsStyle={"danger"}>{this.props.data.amount}</Label></h3>}
          </Col>
        </Row>
        <Row>
          <Col xs={7} sm={8} smOffset={2}>
            <Button block={true} className="button-accept"
              onClick={this.openAcceptRequestModal}>ACCEPT</Button>
          </Col>
          <Col xs={5} sm={8} smOffset={2}>
            <Button block={true} className="button-decline"
              onClick={this.openDeclineRequestModal}>DECLINE</Button>
          </Col>
        </Row>
        <ModalContainer show={this.state.showModal}
          close={this.closeModal}
          onHide={this.closeModal}
          modalHeaderText="Request"
          modalBodyText={"Confirm payment to " + this.props.data.with}
          modalNumberValue={this.props.data.amount}
          confirmCallback={this.handleConfirm} />
      </div>
    );
  }
}

export default Request;
