import React, { Component } from 'react';
import {FormGroup, ControlLabel, FormControl,
  Grid, Row, Col, Button, Image} from 'react-bootstrap';
import NavBackBar from './NavBackBar';
import '../css/ProfilePage.css';
import avatar from '../images/avatar.png';

class ProfilePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: this.props.data.name,
      email: this.props.data.email,
      updated: false,
      nameValid: null,
      emailValid: null
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleApply = this.handleApply.bind(this);
    this.simpleFormValidation = this.simpleFormValidation.bind(this);
  }

  handleChange(e) {
    const name = e.target.name;
    const value = e.target.value;
    this.setState({
      [name]: value,
    });
  }

  handleApply(e) {
    e.preventDefault();
    if (!this.simpleFormValidation()) {
      this.setState({updated: false})
      return;
    }
    this.props.updateName(this.state.name);
    this.props.updateEmail(this.state.email);
    this.setState({updated: true});
  }

  simpleFormValidation() {
    let message = "";
    let nameValid, emailValid = null;

    // Check conditions
    if (this.state.name === "") {
      message = "Please fill all fields.";
      nameValid = "error";
    }
    if (this.state.email === "") {
      message = "Please fill all fields.";
      emailValid = "error";
    }

    // Show feedback and return if valid
    if (message !== "") {
      this.setState({
        nameValid,
        emailValid
      })
      this.props.createFeedback(message, "error")
      return false
    }
    return true;
  }

  render() {
    const {name, email} = this.state;
    const updatedText = this.state.updated ? <h5 className="updated-text">
      Profile updated</h5> : null;
    return (
      <div>
      <NavBackBar currentPage={"Your Profile"}/>
          <Grid>
            <Row className="no-border avatar">
              <Col xs={8} xsOffset={2}>
                <Image src={avatar} rounded responsive />
              </Col>
            </Row>
            <Row className="no-border">
              <Col cs={6} xsOffset={2}>
                <Button type="button" className="button-change-picture">
                  Change picture
                </Button>
              </Col>
            </Row>
            <Row className="no-border">
              <Col xs={10} xsOffset={1}>
                <FormGroup validationState={this.state.nameValid}>
                <ControlLabel>Name</ControlLabel>
                <FormControl
                  type="text"
                  name="name"
                  value={name}
                  onChange={this.handleChange}
                />
                <FormControl.Feedback />
              </FormGroup>
              </Col>
            </Row>
            <Row className="no-border">
              <Col xs={10} xsOffset={1}>
                <FormGroup validationState={this.state.emailValid}>
                <ControlLabel>Email</ControlLabel>
                <FormControl
                  type="email"
                  name="email"
                  value={email}
                  onChange={this.handleChange}
                />
                <FormControl.Feedback />
                </FormGroup>
              </Col>
            </Row>
            <Row className="no-border">
              <Col xs={10} xsOffset={1}>
                <FormGroup>
                  <Button block={true} className="button-apply"
                    onClick={this.handleApply}>
                    Apply
                  </Button>
                 </FormGroup>
              </Col>
            </Row>
            {updatedText}
          </Grid>
        </div>
    );
  }
}

export default ProfilePage;
