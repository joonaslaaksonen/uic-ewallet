import React, {Component} from 'react';
import {Row, Col, Label} from 'react-bootstrap';

class Transaction extends Component {
  render() {
    const {amount, type} = this.props.data;
    let amountLabel;
    if (type === 'sent') {
      amountLabel = <h3><Label bsStyle="danger">{amount}</Label></h3>
    }
    else {
      amountLabel = <h3><Label bsStyle="success">{amount}</Label></h3>
    }

    return(
      <Row className="show-grid">
        <Col sm={8} xs={8} className="text-left">
          {this.props.data.title} for {this.props.data.with}<br/>
          {this.props.data.date}
        </Col>
        <Col sm={4} xs={4} className="text-right">
          {amountLabel}
        </Col>
      </Row>
    );
  }
}

export default Transaction;
