import React, {Component} from 'react';
import {Row, Col} from 'react-bootstrap';
import {LinkContainer} from 'react-router-bootstrap';
import AngleRight from 'react-icons/lib/fa/angle-right';
import '../css/CommunityBar.css';

class CommunityBar extends Component {
  render() {
    return(
      <LinkContainer to="/communities">
        <Row className="show-grid community-bar">
          <Col xs={10} sm={10} className="text-left">
            <strong>Welcome to {this.props.community} community!</strong>
          </Col>
          <Col xs={2} sm={2} className="text-right">
            <AngleRight size={28}/>
          </Col>
        </Row>
      </LinkContainer>
    );
  }
}

export default CommunityBar;
