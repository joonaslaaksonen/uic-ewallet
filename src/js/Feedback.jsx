import React, { Component } from 'react';
import {Row, Col, Button} from 'react-bootstrap';

import '../css/Feedback.css';

class Feedback extends Component {
  constructor(props){
    super(props);
    this.removeFeedback = this.removeFeedback.bind(this);
  }
  removeFeedback() {
    this.props.removeFeedback(this.props.id)
  }
  render() {
    return (
      <Row className="feedback">
        <Col xs={10}>
          <span>{this.props.message}</span>
        </Col>
        <Col xs={2}>
          <Button onClick={this.removeFeedback}>x</Button>
        </Col>
      </Row>
    );
  }
}

export default Feedback;