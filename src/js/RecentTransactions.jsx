import React, {Component} from 'react';
import {Row, Col} from 'react-bootstrap';
import Transaction from './Transaction';
import AngleRight from 'react-icons/lib/fa/angle-right';
import {LinkContainer} from 'react-router-bootstrap';

class RecentTransactions extends Component {
  createElements() {
    const {transactions} = this.props;
    let recents = []
    transactions.forEach((t) => {
      if (t.type !== 'pending') {
        recents.push(t);
      }
    });
    recents.sort(this.compareTransactions);
    const n = recents.length < 3 ? transactions.length : 3;
    const transactionElems =[]
    recents.forEach((data, index) => {
      if (data.type !== 'pending' && data.type !== 'declined') {
      transactionElems.push(<Transaction data={data} key={index} />);
      }
    });

    return transactionElems.slice(0,n);
  }

  compareTransactions(a,b) {
    // Compares date properties of parameters. Date properties must be in form "dd.mm.yyyy"
    let dateA = new Date(a.date.split('.')[2], a.date.split('.')[1],a.date.split('.')[0]);
    let dateB = new Date(b.date.split('.')[2], b.date.split('.')[1],b.date.split('.')[0]);
    if(dateA > dateB) {
      return -1;
    }
    if(dateA < dateB){
      return 1;
    }
    return 0;
  }


  render() {
    const transactionElems = this.createElements();
    return(
      <div>
        <Row>
          <Col xs={12} sm={12} className="text-left">
            <strong>Recent Transactions</strong>
          </Col>
        </Row>
        {transactionElems}
        <LinkContainer to="/transactions">
          <Row>
            <Col xs={6} sm={6} className="text-left">
              More transactions
            </Col>
            <Col xs={6} sm={6} className="text-right">
              <AngleRight size={28}/>
            </Col>
          </Row>
        </LinkContainer>
      </div>
    );
  }
}

export default RecentTransactions;
