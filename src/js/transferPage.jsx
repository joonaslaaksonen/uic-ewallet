import React, { Component } from 'react';
import {Row, Col, Button, Form, FormGroup, FormControl } from 'react-bootstrap';
import NavBackBar from './NavBackBar.jsx';
import '../css/TransferPage.css';

class TransferPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mode: "send",

      hours: "",
      description: "",
      user: "",

      hoursValid: null,
      descriptionValid: null,
      userValid: null,
    }
    this.switchMode = this.switchMode.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.sendTime = this.sendTime.bind(this);
    this.createRequest = this.createRequest.bind(this);
    this.simpleFormValidation = this.simpleFormValidation.bind(this);
  }
  switchMode(e) {
    if (e.target.className.toLowerCase().indexOf("active") === -1) {
      this.setState({
        mode: this.state.mode === "send" ? "request" : "send",

        hours: "",
        description: "",
        user: "",

        hoursValid: null,
        descriptionValid: null,
        userValid: null,
      })
    }
  }
  handleChange(e) {
    const name = e.target.name;
    const value = e.target.value;
    this.setState({
      [name]: value,
    });
  }
  handleSubmit(e) {
    e.preventDefault();
    if (!this.simpleFormValidation()) {
      return;
    }
    if(this.state.mode === "send") {
      this.props.showModal(
        this.sendTime,
        "Confirm your payment",
        "Sending " + this.state.hours + " hours to " + this.state.user + " for completing " + this.state.description,
        this.state.hours,
      )
    }
    else if(this.state.mode === "request") {
      this.props.showModal(
        this.createRequest,
        "Confirm your request",
        "Requesting " + this.state.hours + " hours from " + this.state.user + " for completing " + this.state.description,
        this.state.hours,
      )
    }
  }
  simpleFormValidation() {
    let message = "";
    let hoursValid, descriptionValid, userValid = null;

    // Check conditions
    if (this.state.hours === "") {
      message = "Please fill all fields.";
      hoursValid = "error";
    }
    if (this.state.description === "") {
      message = "Please fill all fields.";
      descriptionValid = "error";
    }
    if (this.state.user === "") {
      message = "Please fill all fields.";
      userValid = "error";
    }
    else if (this.state.hours > this.props.data.currentBalance) {
      message = "You don't have enough hours to sent.";
      hoursValid = "error";
    }

    // Show feedback and return if valid
    if (message !== "") {
      this.setState({
        hoursValid: hoursValid,
        descriptionValid: descriptionValid,
        userValid: userValid,
      })
      this.props.createFeedback(message, "error")
      return false
    }
    return true;
  }

  sendTime() {
    this.props.sendTime(this.state.hours, this.state.description, this.state.user);
    this.setState({
      hours: "",
      description: "",
      user: "",
    })
  }
  createRequest() {
    this.props.createRequest(this.state.hours, this.state.description, this.state.user);
    this.setState({
      hours: "",
      description: "",
      user: "",
    })
  }
  render() {
    return (
      <div className="transfer-container">
        <NavBackBar currentPage={"Send or Request Time"}/>
        <Row className="tab-selector">
          <Col xs={6} className={this.state.mode === "send" ? "active":false} onClick={this.switchMode} ref="send">
            Send
          </Col>
          <Col xs={6} className={this.state.mode === "request" ? "active":false} onClick={this.switchMode} ref="request">
            Request
          </Col>
        </Row>
        <Row className="transfer-form">
          <Col xs={10} xsOffset={1}>
            <Form onSubmit={this.handleSubmit}>
              <FormGroup validationState={this.state.hoursValid}>
                <FormControl
                  type="number"
                  name="hours"
                  value={this.state.hours}
                  placeholder="Hours"
                  onChange={this.handleChange}
                />
                </FormGroup>
                <FormGroup validationState={this.state.descriptionValid}>
                  <FormControl
                    type="text"
                    name="description"
                    value={this.state.description}
                    placeholder="Job description"
                    onChange={this.handleChange}
                  />
                </FormGroup>
                <FormGroup validationState={this.state.userValid}>
                  <FormControl
                    type="text"
                    name="user"
                    value={this.state.user}
                    placeholder="User"
                    onChange={this.handleChange}
                  />
               </FormGroup>
               <FormGroup>
                  <Button type="submit">
                    {this.state.mode.toUpperCase()}
                  </Button>
                </FormGroup>
              </Form>
          </Col>
        </Row>
      </div>
    );
  }
}

export default TransferPage;
