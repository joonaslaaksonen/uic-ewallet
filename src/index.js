import React from 'react'
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Redirect} from 'react-router-dom'

import App from './js/App.jsx';
import MainPage from './js/mainPage.jsx';
import ProfilePage from './js/profilePage.jsx';
import LoginPage from './js/loginPage.jsx';
import ChooseCommunity from './js/ChooseCommunity';
import TransferPage from './js/transferPage';
import AllTransactions from './js/AllTransactions';
import AllRequests from './js/AllRequests';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';
import './css/index.css';


const Index = () => (
  <Router>
    <App account='0'>
      <Route path="/login" component={Login}/>
      <PrivateRoute exact path="/" component={MainPage}/>
      <PrivateRoute path="/profile" component={ProfilePage}/>
      <PrivateRoute path="/communities" component={ChooseCommunity}/>
      <PrivateRoute path="/transfer" component={TransferPage}/>
      <PrivateRoute path="/transactions" component={AllTransactions}/>
      <PrivateRoute path="/requests" component={AllRequests}/>
    </App>
  </Router>
)

const fakeAuth = {
  isAuthenticated: false,
  authenticate(cb) {
    this.isAuthenticated = true
    setTimeout(cb, 100) // fake async
  },
  signout(cb) {
    this.isAuthenticated = false
    setTimeout(cb, 100)
  }
}

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={props => (
    fakeAuth.isAuthenticated ? (
      <Component {...rest} {...props}/>
    ) : (
      <Redirect to={{
        pathname: '/login',
        state: {
          from: props.location,
        }
      }}
      />
    )
  )}/>
)


class Login extends React.Component {
  render() {
    const { from } = this.props.location.state || { from: { pathname: '/' } }
    return (
      <div>
        <LoginPage
          fakeAuth={fakeAuth}
          location={from}
          />
      </div>
    )
  }
}

ReactDOM.render(<Index/>, document.getElementById('root'));



// /*
// -----------
// STRUCTURE
// -----------
// App
// - Login
// - Main
//   - sendTimePage
//   - requestTimePage
//   - profilePage
// Components
// - Navbar
// - Footer
// */
